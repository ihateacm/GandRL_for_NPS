#!/usr/bin/env bash

FILENAME=$1
LINE_COUNT=$( cat $FILENAME | wc -l | awk '{print $1"/10"}' | bc )

head $FILENAME -n $LINE_COUNT
